package C3p0;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Test1 {
    public static void main(String[] args) {
        //{"java":"name","age":20}

        JSONObject object=new JSONObject();
        object.put("uesrname","java");//添加元素
        object.put("age",20);

        System.out.println(object.toString());


        //{"Person":{"pageCode":1,"java":12}}
//        JSONObject object1=new JSONObject();
//
//        Person person=new Person("rose",12);
//
//        object1.put("Person",person);
//        System.out.println(object1.toString());


//person对象转换json对象{"age":12,"books":[],"username":"rose"}
        //将person实例化
        Person person1=new Person("rose",12);
        //利用JSONObject.fromObject输出person
        JSONObject object2=JSONObject.fromObject(person1);
        System.out.println(object2.toString());

//集合转json数组  [{"age":13,"books":[],"username":"rose"},{"age":12,"books":[],"username":"java"}]
        List<Person> list=new ArrayList<>();
        list.add(new Person("rose",13));
        list.add(new Person("java",12));

        JSONArray array=new JSONArray();
        array.addAll(list);
        System.out.println(array.toString());


//{"age":20,"books":["三国演义","三国演义"],"username":"张三"}
        Person person=new Person();
        person.setUsername("张三");
        person.setAge(20);

        List<String> books=new ArrayList<>();
        books.add("三国演义");
        books.add("三国演义");

        person.setBooks(books);
        JSONObject object1=JSONObject.fromObject(person);
        System.out.println(object1.toString());

    }
}
