package C3p0;

import java.util.List;

public class Person {
    private String username;
    private int age;
    private List<String>books;

    public Person(String username, int age) {
        this.username = username;
        this.age = age;
    }

    public Person(String username, int age, List<String> books) {
        this.username = username;
        this.age = age;
        this.books = books;
    }

    public Person() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public List<String> getBooks() {
        return books;
    }

    public void setBooks(List<String> books) {
        this.books = books;
    }
}
